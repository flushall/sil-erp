package org.sil.service.impl;

import com.alibaba.fastjson.JSONArray;
import org.sil.service.DepotService;
import org.springframework.stereotype.Service;

/**
 * 仓库业务实现层
 *
 * @author:gyrSil
 * @since:Created in 2022/12/6 22:06
 */
@Service
public class DepotServiceImpl implements DepotService {

    @Override
    public JSONArray findDepotByCurrentUser() {
        return null;
    }
}
