package org.sil.service;

import com.alibaba.fastjson.JSONArray;
/**
 * 仓库业务层
 *
 * @author:gyrSil
 * @since:Created in 2022/12/6 22:06
 */
public interface DepotService {

    //获取当前用户拥有权限的仓库列表
    JSONArray findDepotByCurrentUser();

}
