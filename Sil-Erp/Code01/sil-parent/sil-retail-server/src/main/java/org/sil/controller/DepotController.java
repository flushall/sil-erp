package org.sil.controller;


import com.alibaba.fastjson.JSONArray;
import com.sil.lls.util.ReturnData;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.sil.service.DepotService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;


/**
 * @Description: 仓库入口层
 * @author gyrSil
 * @date 2022/12/6 21:54
 */
@RestController
@RequestMapping(value = "/depot")
@Api(value = "DepotController" ,tags = {"仓库管理"})
public class DepotController {
    private Logger logger = LoggerFactory.getLogger(DepotController.class);

    @Autowired
    private DepotService depotService;
    /**
     * @Description: TODO
     * @param request:
     * @return BaseResponseInfo
     * @author gyrSil
     * @date 2022/12/6 22:02
     */
    @GetMapping(value = "/findDepotByCurrentUser")
    @ApiOperation(value = "获取当前用户拥有权限的仓库列表")
    public ReturnData findDepotByCurrentUser(HttpServletRequest request) throws Exception{
        try {
            return new ReturnData("0000","查询成功",depotService.findDepotByCurrentUser());
        } catch (Exception e) {
            e.printStackTrace();
            return new ReturnData("9999","查询失败："+e.getCause(),null);
        }
    }
}
