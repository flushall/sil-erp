package org.sil.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 单据入口层
 * @author:gyrSil
 * @since:Created in 2022/12/6 22:15
 */
@RestController
@RequestMapping(value = "/bill")
@Api(value = "BillController" ,tags = {"单据管理"})
public class BillController {



}
