package org.sil;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.io.IOException;

/**
 * TODO
 *
 * @author:gyrSil
 * @since:Created in 2022/12/6 21:13
 */
@Slf4j
@EnableDiscoveryClient
@SpringBootApplication
//@MapperScan("org.sil.erp.datasource.mappers")
@ServletComponentScan
@EnableScheduling
public class RetailServerApplication {

    public static void main(String[] args) throws IOException {
        ConfigurableApplicationContext context = SpringApplication.run(RetailServerApplication.class, args);
        log.info("零售服务启动成功！");
    }
}
