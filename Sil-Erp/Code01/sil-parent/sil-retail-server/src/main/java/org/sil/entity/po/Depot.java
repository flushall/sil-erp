package org.sil.entity.po;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import lombok.Data;

import java.math.BigDecimal;

/**
 * TODO
 *
 * @author:gyrSil
 * @since:Created in 2022/12/6 21:50
 */
@Data
@TableName("SIL_DEPOT")
@ApiModel(value = "仓库实体类", description = "仓库实体类")
public class Depot {
    private Long id;

    private String name;

    private String address;

    private BigDecimal warehousing;

    private BigDecimal truckage;

    private Integer type;

    private String sort;

    private String remark;

    private Long principal;

    private Boolean enabled;

    private Long tenantId;

    private String deleteFlag;

    private Boolean isDefault;
}
