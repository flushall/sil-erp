package com.sil.lls.util;

import lombok.Data;

import java.io.Serializable;

/**
 * 前台返回信息对象
 *
 * @author:gyrSil
 * @since:Created in 2022/11/16 23:24
 */
@Data
public class ReturnData <T> implements Serializable {
    private static final long serialVersionUID = 1L;

    private String status_code;
    private String message;
    private T data;

    public ReturnData(){
        this.status_code = "0000";
    }

    public ReturnData(T data) {
        this.status_code = "0000";
        this.data = data;
    }

    public ReturnData(String status_code, String message) {
        this.status_code = status_code;
        this.message = message;
    }
    public ReturnData(String status_code,String message,T data){
        this.status_code = status_code;
        this.message = message;
        this.data = data;
    }





}
