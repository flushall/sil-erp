# m2下nacos整合springboot

## 版本适配图

![image-20221114233020866](/Users/yrgao/Library/Application Support/typora-user-images/image-20221114233020866.png)

![image-20221114233007482](/Users/yrgao/Library/Application Support/typora-user-images/image-20221114233007482.png)

## 版本管理

springboot：2.3.12.RELEASE

Spring-cloud:Hoxton.SR12

Nacos:2.0.3

## Nacos

1. 使用docker安装nacos

```shell
docker pull zhusaidong/nacos-server-m1:2.0.3   #要带上版本号，默认latest拉取不下来
```

2. 启动脚本（nacos2.0以上需要开放nacos端口+偏移量1000，1001）

```shell
docker run -e MODE=standalone -d -p 8848:8848 -p 9848:9848 -p 9849:9849 --name nacos-server-new f1d91cde7e60 #f1d91cde7e60为镜像ID
```

3. 连接外部数据库/云数据库需要加上下列启动参数，或在nacos容器内部/conf/application.properties配置数据库信息

```properties
MYSQL_SERVICE_HOST=数据库ip
MYSQL_SERVICE_PORT=端口（默认3306）
MYSQL_SERVICE_DB_NAME=数据库名
MYSQL_SERVICE_USER=用户名
MYSQL_SERVICE_PASSWORD=密码
```

4. docker下进入对应容器

```shell
$ docker exec -it 38b0f57a918b /bin/bash #38b0f57a918b:容器id ， 有的容器是在/bin/sh文件夹
```

5. 服务端和客户端需要严格按照版本适配图中的版本进行统一
6. 服务端引入

```xml
<dependency>
        <groupId>com.alibaba.cloud</groupId>
        <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
        <!--解决nacos-client2.0报错的问题-->
        <!--<exclusions>
        <exclusion>
        <groupId>com.alibaba.nacos</groupId>
        <artifactId>nacos-client</artifactId>
        </exclusion>
        </exclusions>-->
</dependency>
```

7. 服务端配置文件增加

```yml
spring:
  application:
    name: sil-erp-syntheic
  cloud:
    nacos:
      discovery:
        namespace: public
        server-addr: 127.0.0.1:8848
```

