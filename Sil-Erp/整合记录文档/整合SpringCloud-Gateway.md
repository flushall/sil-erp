# 整合SpringCloud-Gateway

## 版本兼容

考虑到版本兼容问题，延用sil-parent的cloud，boot版本。

## 依赖

```xml
<dependency>
      <groupId>com.alibaba.cloud</groupId>
      <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
      <!--解决nacos-client2.0报错的问题-->
      <!--<exclusions>
      <exclusion>
      <groupId>com.alibaba.nacos</groupId>
      <artifactId>nacos-client</artifactId>
      </exclusion>
      </exclusions>-->
</dependency>

<dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-starter-gateway</artifactId>
</dependency>

<dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <scope>test</scope>
</dependency>

<dependency>
      <groupId>org.springframework.cloud</groupId>
      <artifactId>spring-cloud-dependencies</artifactId>
      <version>${spring-cloud.version}</version>
      <type>pom</type>
      <scope>import</scope>
</dependency>
```

## 配置文件

```yml
server:
  port: 10001

spring:
  application:
    name: sil-gateway
  cloud:
    nacos:
      discovery:
        namespace: public
        server-addr: 127.0.0.1:8848
    gateway:
      discovery:
        #开启服务发现
        enabled: true
        # 将请求路径上的服务名配置为小写
        lowerCaseServiceId: true
      routes:
        - id: silErpSynthetic         # ID唯一，无实际作用
          #第一种：ws(websocket)方式: uri: ws://localhost:9000
          #第二种：http方式: uri: http://localhost:8130/
          #第三种：lb(注册中心中服务名字)方式: uri: lb://brilliance-consumer
          uri: lb://sil-erp-syntheic #当predicates匹配成功后，使用该路由的uri进行服务调用
          predicates:                 # 匹配指定路径下的请求
            - Path=/jshERP-boot/**

```

## 启动类

增加@EnableDiscoveryClient服务发现注解。

其他暂时不做改动。